FROM python:latest

WORKDIR /tmp/build
COPY requirements.txt .
RUN pip install -r requirements.txt